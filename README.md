# IAGOS-CARIBIC

IAGOS-CARIBIC resources

## Getting started

**Check out the [Wiki home page](https://gitlab.kit.edu/kit/imk-asf-top/IAGOS-CARIBIC/-/wikis/home) for more info and directions (links)**

## License

CC BY 4.0 - see LICENSE file in the root directory of the repository.
