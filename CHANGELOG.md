# Changelog

<https://keepachangelog.com/>

Types of changes

- 'Added' for new features.
- 'Changed' for changes in existing functionality.
- 'Deprecated' for soon-to-be removed features.
- 'Removed' for now removed features.
- 'Fixed' for any bug fixes.
- 'Security' in case of vulnerabilities.

## [Unreleased]

## 2025-01-08, v0.0.8

### Added

- comm specs, checksum info

## 2024-12-19, v0.0.7

### Changed

- LabVIEW example update, LV13 and LV21 version

## 2024-11-21, v0.0.6

### Changed

- update comm specs, mastersim docs, LabVIEW utils

## 2024-09-05, v0.0.5

### Changed

- update LabVIEW utils

## v0.0.4 - 2024-05-03

- use CC BY 4.0 license since this repo contains essentially data (documentation, examples, configs etc.)

## v0.0.3 - 2023-07-11

- moved repository to <gitlab.kit.edu>

## v0.0.3 - 2023-06-19

- update docs and communication specs

## v0.0.2 - 2022-11-03

- Master Sim docs rev1

## v0.0.1 - 2022-05-17

- initial set-up
