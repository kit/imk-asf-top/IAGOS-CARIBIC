# source code

### `/go/ntp`

- NTP client in Go
- [NTP client in Zig](https://github.com/FObersteiner/ntp-client)

### `/labview/examples`

- cRIO project using utils

### `/labview/utils`

- helper VIs for handling CARIBIC-3 messages, as sent by the Master Computer

### `/python/message`

- example scripts for creating and parsing CARIBIC-3 messages, as sent by the Master Computer

### `/python/ntp`

- example script for an NTP client in Python

### `/python/udp`

- example scripts for IP/UDP

