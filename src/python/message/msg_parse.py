# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 09:50:54 2020

@author: f.obersteiner
"""
from datetime import datetime, timezone
import socket
import struct
import zlib

# adjust import path to match your project strucutre:
# from .msg_format_codes import fmap, idxmap


# ------------------------------------------------------------------------------
def checkMsg(
    packet: bytes,
    addr_from: tuple,
    addr_to: tuple,
    fmap: dict,
    idxmap: dict,
    min_pac_len=22,
    verbose=False,
) -> bool:
    """
    Check if a packet contains a valid message according to protocol,
    see CARIBIC@350_CommunicationSpecs
    """
    verboseprint = print if verbose else lambda *a, **k: None
    # make sure sender + receiver address in message:
    seq = (
        socket.inet_aton(addr_from[0])
        + struct.pack(fmap["addr_to_port"], addr_from[1])
        + socket.inet_aton(addr_to[0])
        + struct.pack(fmap["addr_to_port"], addr_to[1])
    )

    # make sure address sequence is found
    if seq in packet:
        packet = packet[packet.index(seq) :]
        # make sure packet contains at least header
        if len(packet) >= min_pac_len:
            msg_len = struct.unpack(fmap["len"], packet[idxmap["len"]])[0]
            # make sure length of packet is sufficient
            if len(packet) >= msg_len:
                # truncate trailing bytes
                if len(packet) > msg_len:
                    packet = packet[:msg_len]
                # make sure checksum is correct:
                if (
                    zlib.crc32(packet[:-4])
                    == struct.unpack(fmap["cs"], packet[idxmap["cs"]])[0]
                ):  # zlib.adler32(packet[:-4])
                    return packet
                else:
                    verboseprint("invalid checksum")
            else:
                verboseprint(
                    f"invalid packet length, {msg_len} specified vs. {len(packet)} bytes found."
                )
        else:
            verboseprint(
                "message too short, minimum {min_pac_len} vs. {len(packet)} bytes found."
            )
    else:
        verboseprint("sender/receiver sequence not found in packet.")

    return False


# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
def parseMsg(
    packet: bytes, fmap: dict, idxmap: dict, data_fmt=None, ts_to_dtobj=False
) -> dict:
    """
    Parse a packet to a dictionary.
    Make sure the packet is a valid message first (run checkMsg).
    """
    content = {
        "addr_from_ip": "",
        "addr_from_port": -1,
        "addr_to_ip": "",
        "addr_to_port": -1,
        "len": -1,
        "ts": -1,
        "type": -1,
        "data": "",
        "cs": -1,
    }

    for k, fmt in fmap.items():
        content[k] = struct.unpack(fmt, packet[idxmap[k]])
        if len(content[k]) == 1:
            content[k] = content[k][0]

    if data_fmt:  # if a format for the data is given, unpack the bytes:
        content["data"] = struct.unpack(data_fmt, packet[idxmap["data"]])
    else:  # else just leave it as a bytes string.
        content["data"] = packet[idxmap["data"]]

    if ts_to_dtobj:
        content["ts"] = datetime.fromtimestamp(content["ts"] / 1e9, tz=timezone.utc)

    return content


# ------------------------------------------------------------------------------
