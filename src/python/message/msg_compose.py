# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 17:38:58 2020

@author: f.obersteiner
"""
import socket
import struct
import time
import zlib


def composeMsg(
    addr_from: tuple,
    addr_to: tuple,
    msg_type: int,
    data: bytes,
    fmap: dict,
    timestamp=None,
) -> bytes:
    """
    Compose a message according to protocol, see CARIBIC@350_CommunicationSpecs

    Parameters
    ----------
    addr_from : tuple
        ("ip address" (str), port (int)).
    addr_to : tuple
        ("ip address" (str), port (int)).
    msg_type : int
        type specifier.
    data : bytes
        the data to transmit.
    fmap : dict
        formatting directives to convert bytes to other types and vice versa.
    timestamp : int, optional
        Unix time in nanoseconds. The default is None.

    Returns
    -------
    bytes
        message in CARIBIC-3 format.

    """

    if timestamp is None:
        timestamp = int(time.time() * 1e9)

    # pack message content to binary string
    packet = (
        socket.inet_aton(addr_from[0])
        + struct.pack(fmap["addr_from_port"], addr_from[1])
        + socket.inet_aton(addr_to[0])
        + struct.pack(fmap["addr_to_port"], addr_to[1])
        + b"\x00\x00"  # place holder bytes for packet length
        + struct.pack(fmap["ts"], timestamp)
        + struct.pack(fmap["type"], msg_type)
        + data
    )

    # insert packet length, including checksum bytes
    packet = packet[:12] + struct.pack(fmap["len"], len(packet) + 4) + packet[14:]

    # add checksum
    # zlib.adler32(packet))
    packet += struct.pack(fmap["cs"], zlib.crc32(packet))

    return packet


# ------------------------------------------------------------------------------
