# format codes for struct, for each meassage part
# ! -> network (= big-endian)
# see https://docs.python.org/3/library/struct.html#format-characters
fmap = {
    "addr_from_ip": "!BBBB",  # 4 bytes, uint32
    "addr_from_port": "!H",  # 2 bytes, uint16
    "addr_to_ip": "!BBBB",  # 4 bytes, uint32
    "addr_to_port": "!H",  # 2 bytes, uint16
    "len": "!H",  # 2 bytes, uint16
    "ts": "!q",  # 8 bytes,  int64
    "type": "!B",  # 1 byte,  uint8
    "cs": "!I",
}  # 4 bytes, unit32


# index positions for message parts
idxmap = {
    "addr_from_ip": slice(0, 4),
    "addr_from_port": slice(4, 6),
    "addr_to_ip": slice(6, 10),
    "addr_to_port": slice(10, 12),
    "len": slice(12, 14),
    "ts": slice(14, 22),
    "type": slice(22, 23),
    "data": slice(23, -4),
    "cs": slice(-4, None),
}
