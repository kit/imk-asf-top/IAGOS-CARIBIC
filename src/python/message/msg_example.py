# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 14:03:15 2020

@author: f.obersteiner
"""
from datetime import datetime
import random
import socket
import struct
import time
import zlib

from msg_format_codes import fmap, idxmap


# --- helper ---
def prettyprint_hex(packet):
    assert len(packet.hex()) % 2 == 0
    return "0x" + ", 0x".join(
        packet.hex()[i : i + 2].upper() for i in range(0, len(packet.hex()), 2)
    )


# ---

# define message content
addr_from = ("192.168.232.64", 16064)
addr_to = ("192.168.232.1", 16001)

msg_type, status_str = 1, "SB"

t = int(time.time() * 1e9)  # now, Unix time, nanoseconds resolution
t = int(datetime.fromisoformat("2021-03-07T00:00:00+00:00").timestamp() * 1e9)


# pack message content to binary string
packet = (
    socket.inet_aton(addr_from[0])
    + struct.pack(fmap["addr_from_port"], addr_from[1])
    + socket.inet_aton(addr_to[0])
    + struct.pack(fmap["addr_to_port"], addr_to[1])
    + b"\x00\x00"
    + struct.pack(fmap["ts"], t)  # place holder bytes for packet length
    + struct.pack(fmap["type"], msg_type)
    + status_str.encode("ASCII")
)

# insert packet length, including checksum bytes
packet = packet[:12] + struct.pack(fmap["len"], len(packet) + 4) + packet[14:]

# add checksum
packet += struct.pack(fmap["cs"], zlib.crc32(packet))

# for b in packet:
#     print(f"x{b:02X}", end=' ')

# add some random bytes at beginning and end of packet...
packet = (
    bytearray(random.getrandbits(8) for _ in range(random.randrange(10)))
    + packet
    + bytearray(random.getrandbits(8) for _ in range(random.randrange(10)))
)

# ------------------------------------------------------------------------------

# assuming 'packet' is received...

# make sure sender + receiver address in message:
checksequence = (
    socket.inet_aton(addr_from[0])
    + struct.pack("!H", addr_from[1])
    + socket.inet_aton(addr_to[0])
    + struct.pack("!H", addr_to[1])
)

assert checksequence in packet, "checksequence not found"

# truncate preceeding bytes
packet = packet[packet.index(checksequence) :]

# make sure at least header is complete
assert len(packet) >= 22, "incomplete packet"

msg_len = struct.unpack("!H", packet[idxmap["len"]])[0]

# assure correct packet contains full message
assert len(packet) >= msg_len, "incomplete packet"

# truncate message if it has trailing bytes
if len(packet) > msg_len:
    packet = packet[:msg_len]

# assure correct checksum
assert (
    zlib.crc32(packet[:-4]) == struct.unpack(fmap["cs"], packet[idxmap["cs"]])[0]
), "invalid packet checksum"

unpacked = (
    (
        ".".join(
            map(
                str, struct.unpack(fmap["addr_from_ip"], packet[idxmap["addr_from_ip"]])
            )
        ),
        struct.unpack(fmap["addr_from_port"], packet[idxmap["addr_from_port"]])[0],
    ),
    (
        ".".join(
            map(str, struct.unpack(fmap["addr_to_ip"], packet[idxmap["addr_to_ip"]]))
        ),
        struct.unpack(fmap["addr_to_port"], packet[idxmap["addr_to_port"]])[0],
    ),
    struct.unpack(fmap["len"], packet[idxmap["len"]])[0],
    struct.unpack(fmap["ts"], packet[idxmap["ts"]])[0],
    struct.unpack(fmap["type"], packet[idxmap["type"]])[0],
    "".join(map(chr, packet[idxmap["data"]])),
)

print("retrieved message:", unpacked)
print("bytes (hex):", prettyprint_hex(packet))

packetbytes = list(map(hex, packet))
print(packetbytes[:6], "<- sender")
print(packetbytes[6:12], "<- receiver")
print(packetbytes[12:14], "<- packet length")
print(packetbytes[14:22], "<- timestamp")
print(packetbytes[22:23], "<- msg type")
print(packetbytes[23 : len(packetbytes) - 4], "<- data")
print(packetbytes[len(packetbytes) - 4 :], "<- checksum")

# size as string would approximately double:
# len(';'.join(('192.168.1.1', '192.168.1.64', '24', '0', '1595337472.0', 'SB')))
# >>> 45
