# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 13:35:31 2019

@author: F. Obersteiner, f/obersteiner//kit/edu

----------------
An example script to log the timestamp from an ntp server, using ntplib.
Links: https://github.com/cf-natali/ntplib
       https://pypi.org/project/ntplib/

We are looking to get the time offset of B (the NTP server) relative to
    A (the client that makes the call to the NTP server).

To avoid confusion with local time settings on the client, all timestamps are
    converted to UTC.

----------------
Explanations -
see IETF RFC5905 for details, https://tools.ietf.org/html/rfc5905

p. 23:
Origin Timestamp (org): Time at the client when the request departed
for the server [...]

Receive Timestamp (rec): Time at the server when the request arrived
from the client [...]

Transmit Timestamp (xmt): Time at the server when the response left
for the client [...]

Destination Timestamp (dst): Time at the client when the reply
arrived from the server [...]

p. 29:
The four most recent timestamps, T1 through T4, are used to compute
the offset of B relative to A

theta = T(B) - T(A) = 1/2 * [(T2-T1) + (T3-T4)]

and the round-trip delay

delta = T(ABA) = (T4-T1) - (T3-T2)
----------------
"""
import os
import time
from datetime import datetime, timezone
import ntplib


def logger(path, used_server, ntp_response, sep=",", error=None, verbose=False):
    """
    logger logs ntp server response to csv file.

    Parameters
    ----------
    path : str
        where to locate the logfile.
    used_server : str
        the used ntp server.
    ntp_response : ntplib request class
        return value of ntplib.NTPClient().request()
    sep : str, optional
        colon separator. The default is ','.
    error : str, optional
        an error to log. The default is None.
    verbose : boolean, optional
        set true to print stuff. The default is False.

    Returns
    -------
    None. (logfile)
    """
    # generate file name based on date of client machine
    fname = os.path.join(path, datetime.now().strftime("%Y%m%d") + "_NTP_log.csv")

    # generate file header if file does not exist
    if not os.path.exists(fname):
        mode = "w"
        header = (
            sep.join(
                [
                    "t_log",
                    "server",
                    "t_org",
                    "t_rec",
                    "t_xmt",
                    "t_dst",
                    "net_delay",
                    "offset",
                    "error",
                ]
            )
            + "\n"
        )
    else:
        mode, header = "a", None

    # parse NTP server response
    if not error:
        org = datetime.fromtimestamp(response.orig_time, timezone.utc)  # T1
        rec = datetime.fromtimestamp(response.recv_time, timezone.utc)  # T2
        xmt = datetime.fromtimestamp(response.tx_time, timezone.utc)  # T3
        dst = datetime.fromtimestamp(response.dest_time, timezone.utc)  # T4
        delta = response.delay  # (T4-T1) - (T3-T2)
        theta = response.offset  # 1/2 * [(T2-T1) + (T3-T4)]
    log = datetime.now(timezone.utc)

    with open(fname, mode) as fobj:  # write to file
        if header:
            fobj.write(header)
        if not error:
            line = (
                sep.join(
                    [
                        log.isoformat(),
                        used_server,
                        org.isoformat(),
                        rec.isoformat(),
                        xmt.isoformat(),
                        dst.isoformat(),
                        str(delta),
                        str(theta),
                        "NA",
                    ]
                )
                + "\n"
            )
            fobj.write(line)
        else:
            line = (
                sep.join([log.isoformat(), used_server, *(6 * ["NA"]), str(error)])
                + "\n"
            )
            fobj.write(line)

    if verbose:  # show what has been logged
        print("logged:\n", line, "\nto:\n", fname)


NTP_SERVER = "192.168.232.1"
# NTP_SERVER = '0.uk.pool.ntp.org'

NTP_LOGPATH = "D:/"
UPDATE_RATE = 1  # log every ... seconds

# call ntp client (socket is closed automatically)
client = ntplib.NTPClient()

for i in range(3):  # replace with a while True to keep logging
    try:
        response = client.request(NTP_SERVER, version=3)
        logger(NTP_LOGPATH, NTP_SERVER, response, sep=";", verbose=True)
    except Exception as e:
        print(e)
        logger(NTP_LOGPATH, NTP_SERVER, None, sep=";", error=e, verbose=True)

    time.sleep(UPDATE_RATE)
