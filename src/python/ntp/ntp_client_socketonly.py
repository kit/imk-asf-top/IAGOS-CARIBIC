# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 11:11:08 2019

@author: F. Obersteiner, f/obersteiner//kit/edu

A version of ntp_client_and_logger.py not using ntplib (only for reference)
"""
import time
import socket
import struct
from datetime import date, datetime, timezone

import ntplib  # pip install ntplib

# set IP of NTP
NTP_SERVER = "192.168.232.1"
# NTP_SERVER = '0.de.pool.ntp.org'

# taken from ntplib: determine offset between system and NTP
_SYSTEM_EPOCH = date(*time.gmtime(0)[0:3])
_NTP_EPOCH = date(1900, 1, 1)
NTP_DELTA = (_SYSTEM_EPOCH - _NTP_EPOCH).days * 24 * 3600

VERSION = 3  # NTP version, 1-4
MODE = 3  # 3: client, 4: server
TIMEOUT = 3  # client socket timeout
tx_timestamp = 0


# a helper to calculate floating point Unix time from integer seconds and
# fractional seconds
def calc_time(sec, frac, ntp_delta):
    return (sec + float(frac) / 2**32) - ntp_delta


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client:
    client.settimeout(TIMEOUT)
    dst = time.time() + NTP_DELTA
    xmt0 = [int(dst)]
    xmt0.append(int(abs(dst - xmt0[0]) * 2**32))

    request = struct.pack(
        "!B B B b 11I",
        (0 << 6 | VERSION << 3 | MODE),
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        *xmt0,
    )
    client.sendto(request, (NTP_SERVER, 123))
    data, _ = client.recvfrom(256)
    unpacked = struct.unpack("!B B B b 11I", data)

    ref = calc_time(*unpacked[7:9], 0)
    org = calc_time(*unpacked[9:11], 0)  # T1
    rec = calc_time(*unpacked[11:13], 0)  # T2
    xmt = calc_time(*unpacked[13:15], 0)  # T3

    dst = time.time() + NTP_DELTA

    delta = (dst - org) - (xmt - rec)  # (T4-T1) - (T3-T2) # delay
    theta = ((rec - org) + (xmt - dst)) / 2  # 1/2 * [(T2-T1) + (T3-T4)] # offset

# time comparison, ntplib not used
print(
    datetime.fromtimestamp(time.time(), timezone.utc),
    " --- ",
    datetime.fromtimestamp(xmt - NTP_DELTA, timezone.utc),
)
print(f"net_delay: {delta:.5f}, offset: {theta:.5f}")

# for reference, use ntplib
client = ntplib.NTPClient()
response = client.request(NTP_SERVER, version=4)
print(
    datetime.fromtimestamp(time.time(), timezone.utc),
    " --- ",
    datetime.fromtimestamp(response.tx_time, timezone.utc),
)
print(f"net_delay: {response.delay:.5f}, offset: {response.offset:.5f}")
