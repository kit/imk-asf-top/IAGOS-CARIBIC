# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 11:41:44 2020

@author: f.obersteiner
"""

from datetime import datetime, timezone
import socket
import struct
import time


TIMEOUT = 5  # listen timeout
recv_on = ("192.168.232.64", 16064)
repl_to = ("192.168.232.1", 16064)

reply = "receiver got the message.".encode("utf-8")  # set to None to not send a reply


# Create a UDP socket
with socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM) as UDPSocket:
    UDPSocket.bind(recv_on)
    UDPSocket.settimeout(TIMEOUT)
    print(f"listening on {recv_on}. stop with ctrl+c")

    # listen for data
    # put a while loop here to keep listening, e.g.
    while True:
        try:  # buffer size is 1024 bytes, call will block until timeout expired
            data, addr = UDPSocket.recvfrom(1024)
        except socket.timeout:
            print("timed out.")
        else:
            print(f"received message {data}\nfrom {addr}")
            if reply:
                t = int(time.time() * 1e9)
                ts = datetime.fromtimestamp(t / 1e9, tz=timezone.utc).isoformat(
                    timespec="seconds"
                )
                print(f"sending reply, t = {ts}\n")
                sent = UDPSocket.sendto(
                    struct.pack("!q", t) + data[:8] + reply, repl_to
                )
        time.sleep(1)
