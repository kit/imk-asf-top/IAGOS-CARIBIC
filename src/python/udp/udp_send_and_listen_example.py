# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 11:37:15 2020

@author: f.obersteiner
"""

from datetime import datetime, timezone
import socket
import struct
import time


N = 25  # how many times to send
TIMEOUT = 5  # listen timeout

send_from = ("192.168.232.1", 16064)  # the socket from which to send
send_to = ("192.168.232.64", 16064)  # the target UDP address

message = "hello from sender\n".encode("utf-8")

with socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM) as UDPSocket:
    UDPSocket.bind(send_from)
    UDPSocket.settimeout(TIMEOUT)
    print(f"sending to {send_to}. stop with ctrl+c")

    for _ in range(N):
        t = time.time()
        n = UDPSocket.sendto((struct.pack("!q", int(t * 1e9)) + message), send_to)

        print(f"\nsent {n} bytes from {send_from} to {send_to}")
        print(f'message: {(struct.pack("!q", int(t*1e9)) + message)}')
        print("awaiting reply...")
        try:
            data, addr = UDPSocket.recvfrom(1024)
        except socket.timeout:
            print("timed out.")
        else:
            t = struct.unpack("!q", data[:8])[0]
            ts = datetime.fromtimestamp(t / 1e9, tz=timezone.utc).isoformat(
                timespec="seconds"
            )
            print(f"message {data}\nreceived on {ts}")


print("done.")
