package main

import (
	"fmt"
	"time"

	"github.com/beevik/ntp"
)

func main() {
	var server = "192.168.232.1" // an NTP server on the LAN

	// prompt the current time according to NTP server
	correctTime, err := ntp.Time(server)
	fmt.Println(correctTime, err)

	// with some metadata:
	response, err := ntp.Query(server)
	correctTime = time.Now().Add(response.ClockOffset)
	fmt.Println(response.ClockOffset, correctTime, err)

}
